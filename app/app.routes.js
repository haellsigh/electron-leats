var leats = angular.module('leats');

leats.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider
			.otherwise("/explore");

		$stateProvider
			.state('explore', {
			url: '/explore',
			templateUrl: 'app/components/sidebar/explore.html',
			controller: 'ctrl-explore'
			})
					.state('explore.summoners', {
					url: '/summoners',
					template: '<ui-view />',
					abstract: true
					})
							.state('explore.summoners.list', {
							url: '',
							templateUrl: 'app/components/explore.summoners.html',
							controller: 'ctrl-explore-summoners'
							})
							
							.state('explore.summoners.detail', {
							url: '/:id',
							templateUrl: 'app/components/explore.summoners-detail.html',
							controller: 'ctrl-explore-summoners-detail'
							})
					.state('explore.items', {
					url: '/items',
					template: '<ui-view />',
					abstract: true
					})
							.state('explore.items.list', {
							url: '',
							templateUrl: 'app/components/explore.items.html',
							controller: 'ctrl-explore-items'
							})
							
							.state('explore.items.detail', {
							url: '/items/:id',
							templateUrl: 'app/components/explore.items-detail.html',
							controller: 'ctrl-explore-items-detail'
							});
	}]);