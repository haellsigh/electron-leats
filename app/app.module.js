/* global angular */
var leats = angular.module('leats', [
	'ngAnimate',
	'ui.router',
	'ui.bootstrap',
    'restangular',
	'leatsControllers'//,
	//'leatsServices',
	//'leatsModels'
]);

leats.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
 
                event.preventDefault();
            }
        });
    };
});

angular.module('leatsControllers', []);

leats.config(RestangularProvider) {
    RestangularProvider.setBaseUrl('https://global.api.pvp.net/api/lol/');
    RestangularProvider.setDefaultRequestParams({"api_key": "270303ae-2637-4496-ad31-2b448541dafa"});
    RestangularProvider.setDefaultHttpFields({cache: true});
    
}