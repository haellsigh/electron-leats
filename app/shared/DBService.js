var low = require('lowdb');
var __base = require('__base');
var leatsServices = angular.module('leatsServices', []);

leatsServices.factory('DBService', function () {
	
	/*
	* Make connection with our project lowdb
	*
	*/
	this.db = function (table) {
		var db = low(__base + 'data/projects.json', {
			autosave: true,
			async: true
		});

		return typeof (table == undefined) ? db : db(table);
	};
	
	//Functions here
	
	//End Functions here
	
	return this;

});