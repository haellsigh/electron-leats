angular.module('leatsControllers').controller('ctrl-explore', ['$scope', '$state', '$timeout',
    function ($scope, $state, $timeout) {
        $scope.searchClasses = ['fa-users', 'fa-sitemap'];
        $scope.searchTypes = ['summoners', 'items'];
        
        $scope.classIndex = 0;
        $scope.searchClass = $scope.searchClasses[$scope.classIndex];

        $scope.searchNext = function () {
            $scope.classIndex++;
            if($scope.classIndex >= $scope.searchClasses.length)
                $scope.classIndex = 0;
                
            $scope.searchClass = $scope.searchClasses[$scope.classIndex];
        };
        
        $scope.search = function() {
            
        };
    }
]);